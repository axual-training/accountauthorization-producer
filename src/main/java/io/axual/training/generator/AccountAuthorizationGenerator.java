package io.axual.training.generator;

import io.axual.general.Application;
import io.axual.general.User;
import io.axual.payments.AccountAuthorization;
import io.axual.payments.AccountId;
import io.axual.training.common.Account;
import io.axual.training.common.EventGenerator;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
@ConfigurationProperties(prefix="generator")
public final class AccountAuthorizationGenerator implements
        EventGenerator<AccountId, AccountAuthorization> {

    private final List<Account> accounts;
    private final Random random;
    private final IdStore idStore;
    private final Application application;

    private String applicationVersion;

    public AccountAuthorizationGenerator() {
        accounts = Account.getAll();
        idStore = new IdStore(accounts);
        random = new Random();
        application = Application.newBuilder()
                .setName(getClass().getName())
                .setVersion(applicationVersion)
                .build();
    }

    public AccountAuthorization generate() {
        AccountId identification = accounts.get(random.nextInt(accounts.size())).getIdentification();
        List<User> users = idStore.getRandomIds(identification);
        return AccountAuthorization.newBuilder()
                .setSource(application)
                .setTimestamp(System.currentTimeMillis())
                .setAccountId(identification)
                .setUsers(users)
                .build();
    }

    @Override
    public AccountId getKey(AccountAuthorization record) {
        return record.getAccountId();
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }
}